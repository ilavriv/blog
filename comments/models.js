var mongoose = require('mongoose'),
    Post = require('../blog/models').Post;

var Schema = mongoose.Schema;

var commentSchema = new Schema({
  author: {type: String},
  post: {type: Schema.Types.ObjectId, ref: 'Post'},
  created: {type: Date, default: Date.now},
});


exports.Comment = mongoose.model('Comment', commentSchema);
