'use strict';
$(document).ready(function(){
  $('#summernote').summernote({
    height: 200,
    onImageUpload: function(files, editor, $editable) {
      var imageInput = document.getElementsByClassName('note-image-input');
      var file = imageInput[0].files[0];

      console.log(file);

      var formData = new FormData();
      formData.append('files', file);
      
      var xhr = new XMLHttpRequest();
      xhr.open('POST', '/admin/editorUpload');

      xhr.onreadystatechange = function() {
        if(xhr.readyState === 4) {
          alert('success');
        } else {
          console.log(editor);
          alert('error');
        }
      };

      xhr.send(formData);
      xhr.close();
    }
  });
});
