var express = require('express');

var controllers = require('./controllers');

var core = module.exports = express.Router();

core.get('/', controllers.index);
core.get('/contacts', controllers.contacts);
