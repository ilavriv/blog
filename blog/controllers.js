var models = require('./models');

var Post = models.Post;

exports.posts = function(req, res) {
  Post.find({}, function(err, posts) {
    res.render('blog/posts', {posts: posts});
  });
};

exports.postDetails = function(req, res) {
  Post.findOne({slug: req.params.slug}, function(err, post) {
    if(!err) {
      res.render('blog/post-details', {post: post});
    }
  });
};

exports.search = function(req, res) {
  res.render('blog/posts', {});
};
