var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var tagSchema = new Schema({
  name: {type: String},
  created: {type: Date, default: Date.now}
});


var postSchema = new Schema({
  slug: {type: String, required: true, unique: true},
  title: {type: String, required: true},
  body: {type: String, required: true},
  created: {type: Date, default: Date.now()},
  is_hidden: {type: Boolean, default: true}
});


exports.Post = mongoose.model('Post', postSchema);
exports.Tag = mongoose.model('Tag', tagSchema);
