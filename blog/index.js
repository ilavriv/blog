var express = require('express');

var blog = module.exports = express.Router();

var controllers = require('./controllers');

blog.get('/posts/', controllers.posts);
blog.get('/posts/:slug/', controllers.postDetails);
blog.get('/search/', controllers.search);
