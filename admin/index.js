var express = require('express');
var basicAuth = require('basic-auth-connect');
var busboy = require('connect-busboy');

var admin = module.exports = express.Router();

admin.use(basicAuth(function(user, password) {
  return user == 'ilavriv' && password == 'password'
}));

admin.use(busboy());

var controllers = require('./controllers');

admin.get('/', controllers.adminIndex);

admin.get('/posts', controllers.adminPosts);

admin.get('/posts/create/', controllers.adminPostCreate);
admin.post('/posts/create/', controllers.adminPostCreate);
admin.get('/posts/:id/edit/', controllers.adminPostUpdate);
admin.post('/posts/:id/edit/', controllers.adminPostUpdate);
admin.get('/posts/:id/delete/', controllers.adminPostDelete);

admin.get('/posts/:id/', controllers.adminPostDetails);

admin.post('/editorUpload', controllers.editorUploader);

module.exports = admin;
