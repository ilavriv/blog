/*
 * Controllers for admin
 */


var fs = require('fs');
var path = require('path');
var mongoose = require('mongoose');
var busboy = require('connect-busboy');

var models = require('../blog/models');
var config = require('../config');

var Post = models.Post;
var Tag = models.Tag;


exports.adminIndex = function(req, res) {
  res.send('admin index');
};


exports.adminPosts = function(req, res) {
  Post.find({}, function(err, posts) {
    if(!err) {
      res.render('admin/posts', {posts: posts});
    }
  });
};


exports.adminPostDetails = function (req, res) {
  Post.findOne({_id: req.params.id}, function(err, post) {
    if(!err) {
      res.render('admin/post_details', {post: post});
    }
  });
};


exports.adminPostCreate = function (req, res) {
  if(req.method == 'POST') {
    var post = new Post(req.body);
    post.save(function(err, post) {
      if(!err) {
        res.redirect('/admin/posts/' + post._id);
      } else {
        res.render('admin/create_post', {err: err});
      }
    });
  } else {
      res.render('admin/create_post', {});
  }
};


exports.adminPostUpdate = function (req, res) {
  if(req.method == 'POST') {
    Post.update({_id: req.params.id}, req.body, function(err, post) {
      if(!err) {
        res.redirect('/admin/posts/');
      } else {
        res.render('admin/update_post', {err: err, post: req.body});
      }
    });
  } else {
    Post.findOne({_id: req.params.id}, function(err, post) {
      if(!err) {
        res.render('admin/update_post', {post: post});
      }
    });
  }
};


exports.adminPostDelete = function (req, res) {
  Post.findOne({_id: req.params.id}, function(err, post) {
    if(!err) {
      post.remove(function(err) {
        res.redirect('/admin/posts/');
      });
    }
  });
};


exports.editorUploader = function(req, res) { 
  console.log(req);
  fs.writeFile(file, function(err) {
    if(!err) {
      res.send('ssdsds');
    }
  });
};
