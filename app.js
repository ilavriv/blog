var express = require('express');
var swig = require('swig');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');


var admin = require('./admin');
var core = require('./core');
var blog = require('./blog');

var app = express();


var port = process.env.OPENSHIFT_NODEJS_PORT || 8080;


app.engine('html', swig.renderFile);

app.set('view engine', 'html');
app.set('views', __dirname + '/views');
app.set('view cache', false);

app.use(express.static(__dirname + '/public'));

app.use(bodyParser({keepExtensions: true, uploadDir: __dirname + '/public/uploads'}));


app.use('/', core);
app.use('/admin/', admin);
app.use('/blog/', blog);


swig.setDefaults({cache: false});

//mongoose.connect('mongodb://localhost:27017/blog');

app.listen(port);
console.log('Server run on http://127.0.0.1:3000');
